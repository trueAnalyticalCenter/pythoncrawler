from grab import Grab, GrabError
from grab.document import Document


class Downloader:
    def __init__(self) -> None:
        self.grabber = Grab()

    def get_page(self, url: str) -> Document:
        try:
            return self.grabber.go(url)
        except GrabError:
            return False
