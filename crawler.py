from repository import SitesRepository, PagesRepository, KeywordsRepository, PersonPageRankRepository
from connector import Connector
from downloader import Downloader
import pages_parser
from models import PersonPageRank
import gzip


class Crawler:
    def __init__(self):
        self.unscanned_sitemaps = True
        self.parser = pages_parser
        self.downloader = Downloader()
        self.connector = Connector()
        self.pages_repo = PagesRepository(self.connector)
        self.sites_repo = SitesRepository(self.connector)
        self.person_page_rank_repo = PersonPageRankRepository(self.connector)
        self.keywords_repo = KeywordsRepository(self.connector)
        self.pages_repo.add_robots(self.sites_repo.new_sites)

    def crawl_robots(self):
        sitemaps = {}
        for robots_page in self.pages_repo.robots:
            robots_content = self.downloader.get_page(robots_page.url.replace("http:", "https:")) or \
                             self.downloader.get_page(robots_page.url)
            sitemaps[robots_page.site_id] = self.parser.get_sitemap_from_robots(robots_content)
            self.pages_repo.update_page_scan_date(robots_page)

        self.pages_repo.add_pages((url, site_id) for site_id, url in sitemaps.items())

    def crawl_sitemaps(self):
        while self.unscanned_sitemaps:
            self.unscanned_sitemaps = False
            for sitemap in self.pages_repo.sitemaps:
                sitemap_content = self.downloader.get_page(sitemap.url)
                if not sitemap_content:
                    continue
                if sitemap.url.endswith(".gz"):
                    try:
                        sitemap_content.body = gzip.decompress(sitemap_content.body)
                    except OSError:
                        continue
                self.unscanned_sitemaps = self.unscanned_sitemaps or sitemap_content.text_search("<sitemap>")

                self.pages_repo.add_pages((url, sitemap.site_id) for url in
                                          self.parser.get_urls_from_sitemap(sitemap_content))
                self.pages_repo.update_page_scan_date(sitemap)

    def crawl_ranks(self):
        person_page_ranks_list = []
        for page in self.pages_repo.pages:
            print(page.url)
            page_content = self.downloader.get_page(page.url)
            if not page_content:
                print("ERROR!!  " + page.url)
                continue
            person_page_ranks_list.extend([PersonPageRank(page.id, person_id, rank) for person_id, rank in
                                          self.parser.rank_count(page_content, self.keywords_repo.keywords).items()])
            self.pages_repo.update_page_scan_date(page)
            if len(person_page_ranks_list) > 1000:
                self.person_page_rank_repo.add_persons_page_ranks(person_page_ranks_list)
                person_page_ranks_list = []
        self.person_page_rank_repo.add_persons_page_ranks(person_page_ranks_list)


if __name__ == "__main__":
    crawler = Crawler()
    crawler.crawl_robots()
    crawler.crawl_sitemaps()
    crawler.crawl_ranks()
