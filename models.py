# -*- coding: utf-8 -*-
from datetime import datetime


class Page:
    def __init__(self, page_id: int, url: str, site_id: int) -> None:
        self.id = page_id
        self.site_id = site_id
        self.url = url
        self.found_date_time = datetime.now()
        self.last_scan_date = None


class PersonPageRank:
    def __init__(self, page_id: int, person_id: int, rank: int) -> None:
        self.page_id = page_id
        self.person_id = person_id
        self.rank = rank


class Site:
    def __init__(self, site_id: int, name: str) -> None:
        self.id = site_id
        self.name = name


class Keyword:
    def __init__(self, keyword_id: int, name: str, person_id: int) -> None:
        self.id = keyword_id
        self.name = name
        self.person_id = person_id
