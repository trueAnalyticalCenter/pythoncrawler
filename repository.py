#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from models import Site, Page, Keyword, PersonPageRank
from datetime import datetime
from mysql.connector import Error
from connector import Connector
from typing import Generator, Tuple, Iterable


class Repository:
    def __init__(self, connector: Connector) -> None:
        self.connection = connector.connection

    def _write_entries(self, sql_query: str, args: Iterable[tuple]) -> None:
        try:
            self.cursor = self.connection.cursor()
            self.cursor.executemany(sql_query, args)
            self.connection.commit()
        except Error as err:
            print(err)
        finally:
            self.cursor.close()

    def _get_entries(self, entries_query: str, entry_type: type) -> Generator:
        try:
            self.cursor = self.connection.cursor()
            self.cursor.execute(entries_query)
            self.entries = self.cursor.fetchall()
        except Error as err:
            print(err)
        finally:
            self.cursor.close()

        for entry in self.entries:
            yield entry_type(*entry)

    def _write_entry(self, query: str, args: tuple) -> None:
        try:
            self.cursor = self.connection.cursor()
            self.cursor.execute(query, args)
            self.connection.commit()
        except Error as err:
            print(err)
        finally:
            self.cursor.close()


class SitesRepository(Repository):
    _get_new_sites_sql = """
            SELECT ID, Name
            FROM Sites
            WHERE ID NOT IN (
                SELECT DISTINCT SiteID
                FROM Pages
                );
        """

    def __init__(self, connector: Connector) -> None:
        super().__init__(connector)

    @property
    def new_sites(self) -> Generator[Site, None, None]:
        return self._get_entries(self._get_new_sites_sql, Site)


class PagesRepository(Repository):
    _add_robots_sql = """
        INSERT INTO Pages (SiteID, Url, FoundDateTime)
        VALUES (%s, %s, %s)
        ;
        """

    _get_robots_sql = """
        SELECT ID, Url, SiteID
        FROM Pages
        WHERE LastScanDate IS NULL
        AND Url LIKE '%/robots.txt'
        ;
        """

    _get_sitemaps_sql = """
        SELECT ID, Url, SiteID
        FROM Pages
        WHERE LastScanDate IS NULL
            AND (Url LIKE '%sitemap%')
            ;
        """

    _update_pages_scan_date_sql = """
        UPDATE Pages
        SET LastScanDate = %s
            WHERE ID = %s
            ;
        """

    _add_pages_sql = """
        INSERT INTO Pages (Url, SiteID, FoundDateTime)
        VALUES (%s, %s, %s)
        ;
        """

    _get_pages_sql = """
        SELECT ID, Url, SiteID
        FROM Pages
        WHERE LastScanDate IS NULL
            AND Url NOT LIKE '%.xml'
            AND Url NOT LIKE '%.xml.gz'
            AND Url NOT LIKE '%/robots.txt'
            ;
        """

    def __init__(self, connector: Connector) -> None:
        super().__init__(connector)

    def add_robots(self, new_sites: Iterable[Site]) -> None:
        """Принимает список сайтов и создает для них страницы-сайтмапы"""
        self._write_entries(self._add_robots_sql,
                            [(site.id, site.name+"/robots.txt", datetime.now()) for site in new_sites])

    @property
    def robots(self) -> Generator[Page, None, None]:
        return self._get_entries(self._get_robots_sql, Page)

    @property
    def sitemaps(self) -> Generator[Page, None, None]:
        return self._get_entries(self._get_sitemaps_sql, Page)

    def update_page_scan_date(self, page: Page) -> None:
        self._write_entry(self._update_pages_scan_date_sql, (datetime.now(), page.id))

    def add_pages(self, pages: Iterable[Tuple[str, int]]) -> None:
        self._write_entries(self._add_pages_sql, [(page_url, page_site_id, datetime.now()) for page_url, page_site_id
                                                  in pages])

    @property
    def pages(self) -> Generator[Page, None, None]:
        return self._get_entries(self._get_pages_sql, Page)


class PersonPageRankRepository(Repository):
    _add_persons_page_ranks_sql = """
            INSERT INTO PersonPageRank (PersonID, PageID, Rank)
            VALUES (%s, %s, %s)
            ;
        """

    def __init__(self, connector: Connector) -> None:
        super().__init__(connector)

    def add_persons_page_ranks(self, persons_page_ranks: Iterable[PersonPageRank]):
        self._write_entries(self._add_persons_page_ranks_sql,
                            [(person_page_rank.person_id, person_page_rank.page_id, person_page_rank.rank)
                             for person_page_rank in persons_page_ranks])


class KeywordsRepository(Repository):
    _get_person_keywords_sql = """
        SELECT ID, Name, PersonID
        FROM Keywords
        """

    def __init__(self, connector: Connector) -> None:
        super().__init__(connector)

    @property
    def keywords(self) -> Generator[Keyword, None, None]:
        return self._get_entries(self._get_person_keywords_sql, Keyword)
