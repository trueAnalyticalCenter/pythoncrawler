from collections import defaultdict
from typing import Iterable, Generator
from grab.document import Document
from models import Keyword


def get_urls_from_sitemap(sitemap_content: Document) -> Generator[str, None, None]:
    for link in sitemap_content.select("//loc"):
        yield link.text()


def get_sitemap_from_robots(robots_content: Document) -> str:
    return robots_content.rex_search("http.*sitemap.xml").group(0)


def rank_count(page: Document, keywords: Iterable[Keyword])-> defaultdict:
    rank = defaultdict(int)
    for keyword in keywords:
        person_rank = page.select("//body").text().count(keyword.name)
        if person_rank:
            rank[keyword.person_id] += person_rank
    return rank
